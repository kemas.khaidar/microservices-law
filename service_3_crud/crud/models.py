from django.db import models

class Utang(models.Model):
    KATEGORI_LIST = (
        ('utang', 'utang'),
        ('piutang', 'piutang'),
    )
    nama = models.CharField(max_length=60)
    kategori = models.CharField(max_length=60, choices=KATEGORI_LIST)
    jumlah = models.IntegerField(default=0)
    keterangan = models.TextField()