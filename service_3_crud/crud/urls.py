from django.conf.urls import url
from django.urls import path
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'crud'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='view/', permanent = False), name='$'),
    url(r'view/', view, name='view'),
    url(r'create/', create, name='create'),
    path(r'edit/<id_item>', edit, name='edit'),
    path(r'delete/<id_item>', delete, name='delete'),
    path(r'detail/<id_item>', detail, name='detail'),
    url(r'auth_login/', auth_login, name='auth_login'),
]
