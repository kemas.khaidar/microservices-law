from django.http import HttpResponseRedirect
from django.shortcuts import render
from .auth_utils import ambil_token, ambil_resource
from .models import Utang

def detail(request, id_item):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    utang = Utang.objects.get(id=id_item)
    response['id'] = utang.id
    response['nama'] = utang.nama
    response['kategori'] = utang.kategori
    response['jumlah'] = utang.jumlah
    response['keterangan'] = utang.keterangan
    return render(request, 'detail.html', response)

def view(request):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    models = Utang.objects.all().order_by('-id').values('id', 'nama', 'kategori', 'jumlah', 'keterangan')
    response['utangs'] = models
    return render(request, 'crud.html', response)

def create(request):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    if request.POST:
        nama = request.POST['nama']
        kategori = request.POST['kategori']
        jumlah = int(request.POST['jumlah'])
        keterangan = request.POST['keterangan']
        new_utang = Utang(nama=nama, kategori=kategori, jumlah=jumlah, keterangan=keterangan)
        new_utang.save()
    return HttpResponseRedirect('/view')

def edit(request, id_item):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    if request.POST:
        utang = Utang.objects.get(id=id_item)
        utang.nama = request.POST['nama']
        utang.kategori = request.POST['kategori']
        utang.jumlah = int(request.POST['jumlah'])
        utang.keterangan = request.POST['keterangan']
        utang.save()
    return HttpResponseRedirect('/view')

def delete(request, id_item):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    utang = Utang.objects.get(id=id_item)
    utang.delete()
    return HttpResponseRedirect('/view')

def auth_login(request):
    response = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        resp = ambil_token(username, password, 'oSheev1Woh0OhFieza5ud1ahm9xooNgiuxaefaok', 'Tiongoh1phi0hithai6meiz4aj5roo8Boh0eeb6a')
        json_response = resp.json()
        if 'error' in json_response:
            response['error'] = "error"
            return render(request, 'login.html', response)
        access_token = json_response['access_token']
        request.session['access_token'] = access_token
        models = Utang.objects.all().order_by('-id').values('id', 'nama', 'kategori', 'jumlah', 'keterangan')
        response['utangs'] = models
        return render(request, 'crud.html', response)