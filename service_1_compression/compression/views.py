from django.http import FileResponse
from django.shortcuts import render
from .auth_utils import ambil_token, ambil_resource
from .forms import FileForm

import tempfile, zipfile, os

def compress(request):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    if request.POST:
        fileForm = FileForm(request.POST, request.FILES)
        if fileForm.is_valid:
            file = request.FILES['file']
            fileName = request.FILES['file'].name
            handle_uploaded_file(request.FILES['file'], fileName)
            with zipfile.ZipFile('zipped_file.zip', 'w') as myzip:
                myzip.write(fileName)
            myzip.close()
            os.remove(fileName)
            zip_file = open('zipped_file.zip', 'rb')
            return FileResponse(zip_file)
        else:
            response['error'] = "error"
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'compress.html', response)

def handle_uploaded_file(f, name):
    with open(name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def auth_login(request):
    response = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        resp = ambil_token(username, password, 'oSheev1Woh0OhFieza5ud1ahm9xooNgiuxaefaok', 'Tiongoh1phi0hithai6meiz4aj5roo8Boh0eeb6a')
        json_response = resp.json()
        if 'error' in json_response:
            response['error'] = "error"
            return render(request, 'login.html', response)
        access_token = json_response['access_token']
        request.session['access_token'] = access_token
        fileForm = FileForm()
        response['form'] = fileForm
        return render(request, 'compress.html', response)