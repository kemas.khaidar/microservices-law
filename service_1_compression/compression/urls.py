from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'compression'

urlpatterns = [
	url(r'^$', RedirectView.as_view(url='compress/', permanent = False), name='$'),
    url(r'compress/', compress, name='compress'),
    url(r'auth_login/', auth_login, name='auth_login'),
]
