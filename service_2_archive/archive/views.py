from django.conf import settings
from django.http import FileResponse, Http404
from django.shortcuts import render
from .auth_utils import ambil_token, ambil_resource
from .forms import FileForm
from .models import FileModel

import os

def archive(request):
    response = {}
    if not request.session.get('access_token'):
        return render(request, 'login.html', response)
    resp = ambil_resource(request.session.get('access_token'))
    json_response = resp.json()
    if 'error' in json_response:
        response['expired'] = "expired"
        return render(request, 'login.html', response)
    if request.POST:
        fileForm = FileForm(request.POST, request.FILES)
        if fileForm.is_valid:
            file = request.FILES['file']
            fileName = request.FILES['file'].name
            handle_uploaded_file(request.FILES['file'], fileName)
        else:
            response['error'] = "error"
    models = FileModel.objects.all().order_by('-id').values('id', 'name', 'upload')
    response['files'] = models
    fileForm = FileForm()
    response['form'] = fileForm
    return render(request, 'archive.html', response)

def download(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        file = open(file_path, 'rb')
        return FileResponse(file)
    raise Http404

def handle_uploaded_file(f, name):
    model = FileModel(name=name, upload=f)
    model.save()

def auth_login(request):
    response = {}
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        resp = ambil_token(username, password, 'oSheev1Woh0OhFieza5ud1ahm9xooNgiuxaefaok', 'Tiongoh1phi0hithai6meiz4aj5roo8Boh0eeb6a')
        json_response = resp.json()
        if 'error' in json_response:
            response['error'] = "error"
            return render(request, 'login.html', response)
        access_token = json_response['access_token']
        request.session['access_token'] = access_token
        models = FileModel.objects.all().order_by('-id').values('id', 'name', 'upload')
        response['files'] = models
        fileForm = FileForm()
        response['form'] = fileForm
        return render(request, 'archive.html', response)