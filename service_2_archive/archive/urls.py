from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'archive'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='archive/', permanent = False), name='$'),
    url(r'archive/', archive, name='archive'),
    url(r'auth_login/', auth_login, name='auth_login'),
    url(r'^download/(?P<path>.*)$', download, name='download'),
]
