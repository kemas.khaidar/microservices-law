from django.db import models

class FileModel(models.Model):
    name = models.CharField(max_length=255)
    upload = models.FileField(upload_to ='uploads/')
